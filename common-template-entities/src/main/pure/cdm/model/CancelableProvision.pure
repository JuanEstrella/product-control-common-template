Class {meta::pure::profiles::doc.doc = 'A data defining:  the right of a party to cancel a swap transaction on the specified exercise dates. The provision is for \'walk-away\' cancellation (i.e. the fair value of the swap is not paid). A fee payable on exercise can be specified. As a difference from the FpML construct, the canonical model extends the BuyerSeller class.'} cdm::model::CancelableProvision extends cdm::model::BuyerSeller
[
  ExerciseChoice: ((($this.americanExercise->isNotEmpty() &&
  $this.bermudaExercise->isEmpty()) &&
  $this.europeanExercise->isEmpty()) ||
  (($this.bermudaExercise->isNotEmpty() &&
  $this.americanExercise->isEmpty()) &&
  $this.europeanExercise->isEmpty())) ||
  (($this.europeanExercise->isNotEmpty() &&
  $this.americanExercise->isEmpty()) &&
  $this.bermudaExercise->isEmpty()),
  CancelableProvisionExerciseNoticeReceiverParty: if(
  $this.exerciseNotice.exerciseNoticeReceiver->isNotEmpty(),
  |$this.exerciseNotice.exerciseNoticeReceiver ==
    cdm::model::AncillaryRoleEnum.ExerciseNoticeReceiverPartyCancelableProvision,
  |true
)
]
{
  {meta::pure::profiles::doc.doc = 'American exercise. FpML implementations consists in an exercise substitution group.'} americanExercise: cdm::model::AmericanExercise[0..1];
  {meta::pure::profiles::doc.doc = 'Bermuda exercise. FpML implementations consists in an exercise substitution group.'} bermudaExercise: cdm::model::BermudaExercise[0..1];
  {meta::pure::profiles::doc.doc = 'European exercise. FpML implementations consists in an exercise substitution group.'} europeanExercise: cdm::model::EuropeanExercise[0..1];
  {meta::pure::profiles::doc.doc = 'Definition of the party to whom notice of exercise should be given.'} exerciseNotice: cdm::model::ExerciseNotice[0..1];
  {meta::pure::profiles::doc.doc = 'A flag to indicate whether follow-up confirmation of exercise (written or electronic) is required following telephonic notice by the buyer to the seller or seller\'s agent.'} followUpConfirmation: Boolean[1];
  {meta::pure::profiles::doc.doc = 'The adjusted dates associated with a cancelable provision. These dates have been adjusted for any applicable business day convention.'} cancelableProvisionAdjustedDates: cdm::model::CancelableProvisionAdjustedDates[0..1];
  {meta::pure::profiles::doc.doc = 'Business date convention adjustment to final payment period per leg (swapStream) upon exercise event. The adjustments can be made in-line with leg level BDC\'s or they can be specified separately.'} finalCalculationPeriodDateAdjustment: cdm::model::FinalCalculationPeriodDateAdjustment[*];
  {meta::pure::profiles::doc.doc = 'An initial fee for the cancelable option.'} initialFee: cdm::model::SimplePayment[0..1];
  callingParty: cdm::model::CallingPartyEnum[0..1];
}
