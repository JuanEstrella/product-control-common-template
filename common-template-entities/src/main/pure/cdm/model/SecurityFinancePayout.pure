Class <<cdm::model::metadata.key>> {meta::pure::profiles::doc.doc = 'Security payout specification in case the product payout involves some form of security collateral, as in a securities financing transaction.'} cdm::model::SecurityFinancePayout extends cdm::model::PayoutBase
{
  {meta::pure::profiles::doc.doc = 'Each SecurityLeg represent a buy/sell at different dates, typically 1 near leg and 1 far leg in a securities financing transaction.'} securityFinanceLeg: cdm::model::SecurityFinanceLeg[1..*];
  {meta::pure::profiles::doc.doc = 'Specifies the reference asset.  This is The base type which all products extend (similar to FpML model). Within SecurityPayout we include a condition which validates that the product must be a Security (see below condition \'ProductMustBeSecurity\').'} securityInformation: cdm::model::Product[1];
  {meta::pure::profiles::doc.doc = 'Specifies the Duration Terms of the Security Finance transaction. e.g. Open or Term.'} durationType: cdm::model::Duration[1];
  {meta::pure::profiles::doc.doc = 'A contractual minimum amount which the borrower will pay, regardless of the duration of the loan. A mechanism for making sure that a trade generates enough income.'} minimumFee: cdm::model::Money[0..1];
  {meta::pure::profiles::doc.doc = 'Specifies the terms under which dividends received by the borrower are passed through to the lender.'} dividendTerms: cdm::model::DividendTerms[0..1];
  {meta::pure::profiles::doc.doc = 'Specifies collateral provisions for a Security Finance transaction, including Collateral Type and Margin Percentage.'} collateralProvisions: cdm::model::CollateralProvisions[1];
}
