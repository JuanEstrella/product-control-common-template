Class {meta::pure::profiles::doc.doc = 'Describes the resource that contains the media representation of a business event (i.e used for stating the Publicly Available Information). For example, can describe a file or a URL that represents the event. This type is an extended version of a type defined by RIXML (www.rixml.org).  Rosetta restricts the FpML implementation by not providing the ability to associated a document in hexadecimalBinary or base64Binary until such time that actual use cases will come up.'} cdm::model::Resource
[
  ResourceChoice: (($this.string->isEmpty() &&
  $this.url->isEmpty()) ||
  ($this.string->isNotEmpty() &&
  $this.url->isEmpty())) ||
  ($this.url->isNotEmpty() &&
  $this.string->isEmpty())
]
{
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'The unique identifier of the resource within the event. FpML specifies this element of type resourceIdScheme but with no specified value.'} resourceId: String[1];
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'A description of the type of the resource, e.g. a confirmation.'} resourceType: cdm::model::ResourceTypeEnum[0..1];
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'Indicates the language of the resource, described using the ISO 639-2/T Code.'} language: String[0..1];
  {meta::pure::profiles::doc.doc = 'Indicates the size of the resource in bytes. It could be used by the end user to estimate the download time and storage needs.'} sizeInBytes: Float[0..1];
  {meta::pure::profiles::doc.doc = 'Indicates the length of the resource. For example, if the resource were a PDF file, the length would be in pages.'} length: cdm::model::ResourceLength[0..1];
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'Indicates the type of media used to store the content. mimeType is used to determine the software product(s) that can read the content. MIME Types are described in RFC 2046.'} mimeType: String[0..1];
  {meta::pure::profiles::doc.doc = 'The name of the resource.  It is specified as a NormalizedString in FpML.'} name: String[0..1];
  {meta::pure::profiles::doc.doc = 'Any additional comments that are deemed necessary. For example, which software version is required to open the document? Or, how does this resource relate to the others for this event?'} comments: String[0..1];
  {meta::pure::profiles::doc.doc = 'Provides extra information as string. In case the extra information is in XML format, a CDATA section must be placed around the source message to prevent its interpretation as XML content.'} string: String[0..1];
  {meta::pure::profiles::doc.doc = 'Indicates where the resource can be found, as a URL that references the information on a web server accessible to the message recipient.'} url: String[0..1];
}
