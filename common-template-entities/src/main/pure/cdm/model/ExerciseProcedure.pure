Class {meta::pure::profiles::doc.doc = 'A class describing how notice of exercise should be given. This can be either manual or automatic.'} cdm::model::ExerciseProcedure
[
  ExerciseProcedureChoice: ($this.manualExercise->isNotEmpty() &&
  $this.automaticExercise->isEmpty()) ||
  ($this.manualExercise->isEmpty() &&
  $this.automaticExercise->isNotEmpty())
]
{
  {meta::pure::profiles::doc.doc = 'Specifies that the notice of exercise must be given by the buyer to the seller or seller\'s agent.'} manualExercise: cdm::model::ManualExercise[0..1];
  {meta::pure::profiles::doc.doc = 'If automatic is specified, then the notional amount of the underlying swap not previously exercised under the swaption will be automatically exercised at the expiration time on the expiration date if at such time the buyer is in-the-money, provided that the difference between the settlement rate and the fixed rate under the relevant underlying swap is not less than the specified threshold rate. The term in-the-money is assumed to have the meaning defining in the 2000 ISDA Definitions, Section 17.4 In-the-money.'} automaticExercise: cdm::model::AutomaticExercise[0..1];
  {meta::pure::profiles::doc.doc = 'A flag to indicate whether follow-up confirmation of exercise (written or electronic) is required following telephonic notice by the buyer to the seller or seller\'s agent.'} followUpConfirmation: Boolean[1];
  {meta::pure::profiles::doc.doc = 'Has the meaning defined as part of the 1997 ISDA Government Bond Option Definitions, section 4.5 Limited Right to Confirm Exercise. If present, (i) the Seller may request the Buyer to confirm its intent if not done on or before the expiration time on the Expiration date (ii) specific rules will apply in relation to the settlement mode.'} limitedRightToConfirm: Boolean[0..1];
  {meta::pure::profiles::doc.doc = 'Typically applicable to the physical settlement of bond and convertible bond options. If present, means that the party required to deliver the bonds will divide those to be delivered as notifying party desires to facilitate delivery obligations.'} splitTicket: Boolean[0..1];
}
