Class {meta::pure::profiles::doc.doc = 'A class to specify the reference currency for the purpose of specifying the FX Haircut relating to a posting obligation, as being either the Termination Currency or an FX Designated Currency.'} cdm::model::FxHaircutCurrency
[
  TerminationCurrency: if(
  $this.isTerminationCurrency,
  |$this.fxDesignatedCurrency->isEmpty(),
  |true
),
  FxDesignatedCurrency: if(
  !($this.isTerminationCurrency),
  |$this.fxDesignatedCurrency->isNotEmpty(),
  |true
)
]
{
  {meta::pure::profiles::doc.doc = 'The reference currency for the purpose of specifying the FX Haircut relating to a posting obligation is the Termination Currency when the Boolean value is set to True.'} isTerminationCurrency: Boolean[1];
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'When specified, the reference currency for the purpose of specifying the FX Haircut relating to a posting obligation. The list of valid currencies is not presently positioned as an enumeration as part of the CDM because that scope is limited to the values specified by ISDA and FpML. As a result, implementers have to make reference to the relevant standard, such as the ISO 4217 standard for currency codes.'} fxDesignatedCurrency: String[0..1];
}
