Class <<cdm::model::metadata.key>> {meta::pure::profiles::doc.doc = 'Defines the output of a financial transaction between parties - a Business Event. A Trade impacts the financial position (i.e. the balance sheet) of involved parties.'} cdm::model::Trade
[
  DeliverableObligationsPhysicalSettlementMatrix: if(
  (!($this.contractDetails.documentation.documentationIdentification.contractualMatrix.matrixType ==
    cdm::model::MatrixTypeEnum.CreditDerivativesPhysicalSettlementMatrix) ||
    $this.contractDetails.documentation.documentationIdentification.contractualMatrix.matrixType->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations->isNotEmpty(),
  |((((((((((($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.notSubordinated->isNotEmpty() &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.specifiedCurrency->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.notSovereignLender->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.notDomesticCurrency->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.notDomesticLaw->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.notContingent->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.notDomesticIssuance->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.assignableLoan->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.consentRequiredLoan->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.transferable->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.maximumMaturity->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.notBearer->isNotEmpty()) &&
    (($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.fullFaithAndCreditObLiability->isNotEmpty() ||
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.generalFundObligationLiability->isNotEmpty()) ||
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.revenueObligationLiability->isNotEmpty()),
  |true
),
  ObligationsPhysicalSettlementMatrix: if(
  (!($this.contractDetails.documentation.documentationIdentification.contractualMatrix.matrixType ==
    cdm::model::MatrixTypeEnum.CreditDerivativesPhysicalSettlementMatrix) ||
    $this.contractDetails.documentation.documentationIdentification.contractualMatrix.matrixType->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.obligations->isNotEmpty(),
  |((($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.obligations.notSubordinated->isNotEmpty() &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.obligations.notSovereignLender->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.obligations.notDomesticLaw->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.obligations.notDomesticIssuance->isNotEmpty()) &&
    (($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.obligations.fullFaithAndCreditObLiability->isNotEmpty() ||
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.obligations.generalFundObligationLiability->isNotEmpty()) ||
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.obligations.revenueObligationLiability->isNotEmpty()),
  |true
),
  CreditEventsPhysicalSettlementMatrix: if(
  (!($this.contractDetails.documentation.documentationIdentification.contractualMatrix.matrixType ==
    cdm::model::MatrixTypeEnum.CreditDerivativesPhysicalSettlementMatrix) ||
    $this.contractDetails.documentation.documentationIdentification.contractualMatrix.matrixType->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents->isNotEmpty(),
  |((($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.bankruptcy->isNotEmpty() &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.obligationDefault->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.obligationAcceleration->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.repudiationMoratorium->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.governmentalIntervention->isNotEmpty(),
  |true
),
  RestructuringPhysicalSettlementMatrix: if(
  (!($this.contractDetails.documentation.documentationIdentification.contractualMatrix.matrixType ==
    cdm::model::MatrixTypeEnum.CreditDerivativesPhysicalSettlementMatrix) ||
    $this.contractDetails.documentation.documentationIdentification.contractualMatrix.matrixType->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.restructuring->isNotEmpty(),
  |$this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.restructuring.multipleHolderObligation->isNotEmpty(),
  |true
),
  FloatingAmountEventsMortgages: if(
  (($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation.referenceObligation.security.securityType ==
    cdm::model::SecurityTypeEnum.Debt) &&
    ($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation.referenceObligation.security.debtType.debtClass ==
    cdm::model::DebtClassEnum.AssetBacked)) ||
    (($this.contractDetails.documentation.documentationIdentification.contractualTermsSupplement.contractualTermsSupplementType ==
    cdm::model::ContractualSupplementEnum.CDSonMBS) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.floatingAmountEvents->isNotEmpty()),
  |($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.floatingAmountEvents.failureToPayPrincipal->isNotEmpty() &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.floatingAmountEvents.writedown->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.floatingAmountEvents.impliedWritedown->isNotEmpty(),
  |true
),
  AdditionalFixedPaymentsMortgages: if(
  (($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation.referenceObligation.security.securityType ==
    cdm::model::SecurityTypeEnum.Debt) &&
    ($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation.referenceObligation.security.debtType.debtClass ==
    cdm::model::DebtClassEnum.AssetBacked)) ||
    (($this.contractDetails.documentation.documentationIdentification.contractualTermsSupplement.contractualTermsSupplementType ==
    cdm::model::ContractualSupplementEnum.CDSonMBS) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.floatingAmountEvents->isNotEmpty()),
  |($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.floatingAmountEvents.additionalFixedPayments.interestShortfallReimbursement->isNotEmpty() &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.floatingAmountEvents.additionalFixedPayments.principalShortfallReimbursement->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.floatingAmountEvents.additionalFixedPayments.writedownReimbursement->isNotEmpty(),
  |true
),
  CreditEventsMortgages: if(
  ((($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation.referenceObligation.security.securityType ==
    cdm::model::SecurityTypeEnum.Debt) &&
    ($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation.referenceObligation.security.debtType.debtClass ==
    cdm::model::DebtClassEnum.AssetBacked)) ||
    ($this.contractDetails.documentation.documentationIdentification.contractualTermsSupplement.contractualTermsSupplementType ==
    cdm::model::ContractualSupplementEnum.CDSonMBS)) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents->isNotEmpty(),
  |(((($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.failureToPayPrincipal->isNotEmpty() &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.failureToPayInterest->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.distressedRatingsDowngrade->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.maturityExtension->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.writedown->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.impliedWritedown->isNotEmpty(),
  |true
),
  HedgingParty: if(
  $this.partyRole.role ==
    cdm::model::PartyRoleEnum.HedgingParty,
  |$this.partyRole.role->size() <= 2,
  |true
),
  DeterminingParty: if(
  $this.partyRole.role ==
    cdm::model::PartyRoleEnum.DeterminingParty,
  |$this.partyRole.role->size() <= 2,
  |true
),
  BarrierDerterminationAgent: if(
  $this.partyRole.role ==
    cdm::model::PartyRoleEnum.BarrierDeterminationAgent,
  |$this.partyRole.role->size() <= 1,
  |true
),
  ClearedDate: if(
  $this.clearedDate->isNotEmpty(),
  |$this.clearedDate >=
    $this.tradeDate,
  |true
),
  ContractualProductExists: $this.tradableProduct.product.contractualProduct->isNotEmpty(),
  FpML_cd_1: if(
  $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation->isNotEmpty(),
  |($this.tradeDate <
    $this.tradableProduct.product.contractualProduct.economicTerms.effectiveDate.adjustableDate.unadjustedDate) ||
    ($this.tradeDate <
    $this.tradableProduct.product.contractualProduct.economicTerms.effectiveDate.adjustableDate.adjustedDate),
  |true
),
  FpML_cd_7: if(
  ($this.contractDetails.documentation.documentationIdentification.masterConfirmation->isEmpty() &&
    $this.contractDetails.documentation.documentationIdentification.contractualMatrix->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation->isNotEmpty(),
  |$this.tradableProduct.product.contractualProduct.economicTerms.payout.interestRatePayout.calculationPeriodDates.effectiveDate.adjustableDate.dateAdjustments->isNotEmpty() ||
    ($this.tradeDate <
    $this.tradableProduct.product.contractualProduct.economicTerms.effectiveDate.adjustableDate.adjustedDate),
  |true
),
  FpML_cd_8: if(
  ($this.contractDetails.documentation.documentationIdentification.masterConfirmation->isEmpty() &&
    $this.contractDetails.documentation.documentationIdentification.contractualMatrix->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation->isNotEmpty(),
  |$this.tradableProduct.product.contractualProduct.economicTerms.terminationDate.adjustableDate.dateAdjustments->isNotEmpty(),
  |true
),
  FpML_cd_11: if(
  (($this.contractDetails.documentation.documentationIdentification.masterConfirmation->isEmpty() &&
    $this.contractDetails.documentation.documentationIdentification.contractualMatrix->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation->isNotEmpty()) &&
    ($this.contractDetails.documentation.documentationIdentification.contractualDefinitions ==
    cdm::model::ContractualDefinitionsEnum.ISDA2003Credit),
  |$this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation.allGuarantees->isNotEmpty(),
  |true
),
  FpML_cd_19: if(
  $this.contractDetails.documentation.documentationIdentification.contractualDefinitions ==
    cdm::model::ContractualDefinitionsEnum.ISDA1999Credit,
  |((((($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.creditEventNotice.businessCenter->isEmpty() &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.restructuring.multipleHolderObligation->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.restructuring.multipleCreditEventNotices->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation.allGuarantees->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.indexReferenceInformation->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.substitution->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.modifiedEquityDelivery->isEmpty(),
  |true
),
  FpML_cd_20: if(
  $this.contractDetails.documentation.documentationIdentification.contractualDefinitions ==
    cdm::model::ContractualDefinitionsEnum.ISDA2003Credit,
  |$this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.obligations.notContingent->isEmpty(),
  |true
),
  FpML_cd_23: if(
  ($this.contractDetails.documentation.documentationIdentification.masterConfirmation->isEmpty() &&
    $this.contractDetails.documentation.documentationIdentification.contractualMatrix->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation->isNotEmpty(),
  |$this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.cashSettlementTerms->isNotEmpty() ||
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms->isNotEmpty(),
  |true
),
  FpML_cd_24: if(
  ($this.contractDetails.documentation.documentationIdentification.masterConfirmation->isEmpty() &&
    $this.contractDetails.documentation.documentationIdentification.contractualMatrix->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation->isNotEmpty(),
  |($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.creditEvents.creditEventNotice->isNotEmpty() &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.protectionTerms.obligations->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation.referencePrice->isNotEmpty(),
  |true
),
  FpML_cd_25: if(
  (($this.contractDetails.documentation.documentationIdentification.masterConfirmation->isEmpty() &&
    $this.contractDetails.documentation.documentationIdentification.contractualMatrix->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms->isNotEmpty(),
  |(($this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.settlementCurrency->isNotEmpty() &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.physicalSettlementPeriod->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.escrow->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.physicalSettlementTerms.deliverableObligations.accruedInterest->isNotEmpty(),
  |true
),
  FpML_cd_32: if(
  ((($this.contractDetails.documentation.documentationIdentification.masterConfirmation->isEmpty() &&
    $this.contractDetails.documentation.documentationIdentification.contractualMatrix->isEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.creditDefaultPayout.generalTerms.referenceInformation->isNotEmpty()) &&
    $this.tradableProduct.product.contractualProduct.economicTerms.payout.interestRatePayout.payoutQuantity->isNotEmpty()) &&
    $this.tradableProduct.priceQuantity.quantity.amount->isNotEmpty(),
  |$this.tradableProduct.product.contractualProduct.economicTerms.payout.interestRatePayout.dayCountFraction->isNotEmpty(),
  |true
)
]
{
  {meta::pure::profiles::doc.doc = 'Represents the identifier(s) that uniquely identify a trade for an identity issuer. A trade can include multiple identifiers, for example a trade that is reportable to both the CFTC and ESMA, and then has an associated USI (Unique Swap Identifier) UTI (Unique Trade Identifier).'} tradeIdentifier: cdm::model::Identifier[1..*];
  <<cdm::model::metadata.id>> {meta::pure::profiles::doc.doc = 'Specifies the date which the trade was agreed.'} tradeDate: Date[1];
  {meta::pure::profiles::doc.doc = 'Represents the financial instrument The corresponding FpML construct is the product abstract element and the associated substitution group.'} tradableProduct: cdm::model::TradableProduct[1];
  {meta::pure::profiles::doc.doc = 'Represents the parties to the trade. The cardinality is optional to address the case where the trade is defined within a BusinessEvent data type, in which case the party is specified in BusinessEvent.'} party: cdm::model::Party[*];
  {meta::pure::profiles::doc.doc = 'Represents the role each specified party takes in the trade. further to the principal roles, payer and receiver.'} partyRole: cdm::model::PartyRole[*];
  {meta::pure::profiles::doc.doc = 'Represents how the financial instruments in the trade is to be settled e.g. initial fee, broker fee, up-front cds payment or option premium settlement'} settlementTerms: cdm::model::SettlementTerms[*];
  {meta::pure::profiles::doc.doc = 'Represents information specific to trades that arose from executions.'} executionDetails: cdm::model::ExecutionDetails[0..1];
  {meta::pure::profiles::doc.doc = 'Represents information specific to trades involving contractual products.'} contractDetails: cdm::model::ContractDetails[0..1];
  {meta::pure::profiles::doc.doc = 'Specifies the date on which a trade is cleared (novated) through a central counterparty clearing service.'} clearedDate: Date[0..1];
  {meta::pure::profiles::doc.doc = 'Represents the collateral obligations of a party.'} collateral: cdm::model::Collateral[0..1];
  {meta::pure::profiles::doc.doc = 'Represents a party\'s granular account information, which may be used in subsequent internal processing.'} account: cdm::model::Account[*];
}
